#!/bin/bash

cd ../parsecsv
echo Changing directory to:
pwd

#process files
for file in ../data/Kickstarter_2019-03-14/*.csv
do
  echo The current file is "$file"
  ./a.out "$file" >> all.csv
done
mv all.csv ../data
